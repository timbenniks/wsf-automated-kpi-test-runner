const NodesAPI = require('./endpoints/nodes')
const NodeGroupsAPI = require('./endpoints/node_groups')
const AuthorizationAPI = require('./endpoints/authorize')
const FoldersAPI = require('./endpoints/folders')
const ProductsAPI = require('./endpoints/products')
const TestsAPI = require('./endpoints/tests')
const PerformanceAPI = require('./endpoints/performance')
const OnDemandTestAPI = require('./endpoints/ondemandtest')

module.exports = class {
  constructor({ debug = false } = {}) {
    this.version = 1
    this.config = { debug }

    // mount endpoints;
    this.nodes = new NodesAPI(this.config)
    this.nodeGroups = new NodeGroupsAPI(this.config)
    this.authorization = new AuthorizationAPI(this.config)
    this.folders = new FoldersAPI(this.config)
    this.products = new ProductsAPI(this.config)
    this.tests = new TestsAPI(this.config)
    this.performance = new PerformanceAPI(this.config)
    this.ondemand = new OnDemandTestAPI(this.config)
  }

  setAccessToken(token) {
    this.config.accessToken = token
  }
}
