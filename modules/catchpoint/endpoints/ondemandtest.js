const networking = require('../components/networking')

module.exports = class {
  constructor(config) {
    this.config = config
  }

  create(url) {
    return new Promise((resolve, reject) => {
      if (!this.config.accessToken) return reject('missing access token')
      if (!url) return reject('url is missing')

      const requestParams = networking.createBaseRequestParams(this.config, {
        url: 'ui/api/v1/onDemandTest/0',
        postType: 'form',
        body: `{
          "http_method": {
            "name": "GET",
            "id": 0
          },
          "nodes": [{
            "name": "Amsterdam, NL - Azure",
            "id": 644
          }],
          "monitor": {
            "name": "Chrome",
            "id": 18
          },
          "id": 0,
          "test_type": {
            "name": "Web",
            "id": 0
          },
          "test_url": "${url}"
        }`
      })

      return networking.post(requestParams, resolve, reject)
    })
  }

  getById(id) {
    return new Promise((resolve, reject) => {
      if (!this.config.accessToken) return reject('missing access token')
      if (!id) return reject('instant test id is missing')

      const requestParams = networking.createBaseRequestParams(this.config, {
        url: `ui/api/v1/onDemandTest/${id}`
      })

      return networking.get(requestParams, resolve, reject)
    })
  }
}
