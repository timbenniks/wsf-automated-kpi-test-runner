const Confluency = require('confluency').default

const confluency = new Confluency({
  host: process.env.CONFLUENCE_URL,
  context: '',
  username: process.env.CONFLUENCE_USERNAME,
  password: process.env.CONFLUENCE_PASSWORD,
  authType: 'basic'
})

module.exports = (space, parent, title, content) => new Promise((resolve, reject) => {
  confluency.create({
    space, title, content, parent, representation: 'wiki'
  })
    .then(resolve)
    .catch(reject)
})
