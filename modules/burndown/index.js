// https://jira.e-loreal.com/rest/greenhopper/1.0/rapid/charts/scopechangeburndownchart.json?rapidViewId=851&sprintId=796&statisticFieldId=field_timeestimate&_=1515501581340

const request = require('request')

const path = `https://${process.env.JIRA_URL}/rest/greenhopper/1.0/rapid/charts/scopechangeburndownchart.json`

module.exports = (repidViewId, sprintId) => new Promise((resolve, reject) => {
  const options = {
    url: `${path}?rapidViewId=${repidViewId}&sprintId=${sprintId}&statisticFieldId=field_timeestimate&_=${Date.now()}`,
    auth: {
      user: process.env.JIRA_USERNAME,
      pass: process.env.JIRA_PASSWORD,
      sendImmediately: true
    }
  }

  request(options, (error, response, body) => {
    if (!error && response.statusCode === 200) {
      const data = JSON.parse(body)

      const result = {
        startTime: data.startTime,
        endTime: data.endTime,
        completeTime: data.completeTime || false,
        now: data.now,
        changes: []
      }

      Object.entries(data.changes).forEach(([key, change]) => {
        if (change[0].timeC && change[0].timeC.timeSpent) {
          const usableChange = {
            date: key,
            key: change[0].key,
            timeSpent: change[0].timeC.timeSpent,
            oldEstimate: change[0].timeC.oldEstimate,
            newEstimate: change[0].timeC.newEstimate
          }

          result.changes.push(usableChange)
        }
      })

      resolve(result)
    }
    else {
      reject(error)
    }
  })
})
