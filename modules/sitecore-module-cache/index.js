const request = require('request')

module.exports = (url, username, password) => new Promise((resolve, reject) => {
  const options = {
    url,
    auth: {
      user: username,
      pass: password,
      sendImmediately: true
    }
  }

  request(options, (error, response, body) => {
    if (!error && response.statusCode === 200) {
      const data = JSON.parse(body)

      resolve(data)
    }
    else {
      reject(error)
    }
  })
})
