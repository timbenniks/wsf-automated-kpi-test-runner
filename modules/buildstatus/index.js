// https://wsf-ci.e-loreal.com/view/c-valtech/view/b-masters/view/b-kerastase/job/MASTER-KERASTASE-RELEASE-RELEASE-BUILD/lastBuild/api/json?pretty=true
// buildStatusOutput('b-kerastase', 'MASTER-KERASTASE-RELEASE-RELEASE-BUILD')

const request = require('request')

const path = 'https://wsf-ci.e-loreal.com/view/c-valtech/view/b-masters/view/'

module.exports = (project, job) => new Promise((resolve, reject) => {
  const options = {
    url: `${path}/${project}/job/${job}/lastBuild/api/json?pretty=true`,
    auth: {
      user: process.env.JENKINS_USER,
      pass: process.env.JENKINS_PASSWORD,
      sendImmediately: true
    }
  }

  request(options, (error, response, body) => {
    if (!error && response.statusCode === 200) {
      const data = JSON.parse(body)
      const result = {
        release: data.displayName,
        description: data.fullDisplayName,
        durationType: 'ms',
        duration: data.duration / 60,
        result: data.result
      }

      resolve(result)
    }
    else {
      reject(error)
    }
  })
})
