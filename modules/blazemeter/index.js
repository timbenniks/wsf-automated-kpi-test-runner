const request = require('request')

const path = 'https://a.blazemeter.com:443/api/v4'

// µ is the average response time given by (sum of response times) / (# of responses) in seconds
// A (Average response time score) = 100 - (µ - 3) / 0,1
// e is error rate given by 100 * (# of error responses) / (# of responses) in percentages
// B (Error rate score) = 100 - (e - 1) / 0,1
module.exports = testid => new Promise((resolve, reject) => {
  const options = {
    url: `${path}/tests/${testid}/sessions-summaries?limit=1`,
    auth: {
      user: process.env.BLAZMETER_USER,
      pass: process.env.BLAZMETER_PASSWORD,
      sendImmediately: false
    }
  }

  request(options, (error, response, body) => {
    if (!error && response.statusCode === 200) {
      const data = JSON.parse(body).result
      const avgResponseTimeSeconds = (data.labels[0].responseTime.avg / 1000)
      const errorPercentage = data.labels[0].errors.percent
      const A = 100 - (avgResponseTimeSeconds - 3) / 0.1
      const B = 100 - (errorPercentage - 1) / 0.1
      const score = (A + B) / 2

      const result = {
        id: data.labels[0].id,
        title: data.labels[0].session.name,
        score,
        error: {
          count: data.labels[0].errors.count,
          percent: data.labels[0].errors.percent
        },
        responseTime: {
          min: data.labels[0].responseTime.min,
          max: data.labels[0].responseTime.max,
          avg: data.labels[0].responseTime.avg
        }
      }

      resolve(result)
    }
    else {
      reject(error)
    }
  })
})
