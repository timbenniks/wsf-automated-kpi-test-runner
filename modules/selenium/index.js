// https://wsf-ci.e-loreal.com/view/c-valtech/view/b-masters/view/d-essie/job/ESSIE-Tests-Selenium-UI-Pipelines-Chrome/lastBuild/testReport/api/json?pretty=true
// seleniumOutput('f-garnier', 'GARNIER-Tests-Selenium-UI-Pipelines-Chrome').then()

const request = require('request')

const path = 'https://wsf-ci.e-loreal.com/view/c-valtech/view/b-masters/view/'

module.exports = (project, job) => new Promise((resolve, reject) => {
  const options = {
    url: `${path}/${project}/job/${job}/lastBuild/testReport/api/json?pretty=true`,
    auth: {
      user: process.env.JENKINS_USER,
      pass: process.env.JENKINS_PASSWORD,
      sendImmediately: true
    }
  }

  request(options, (error, response, body) => {
    if (!error && response.statusCode === 200) {
      const data = JSON.parse(body)
      const result = {
        durationType: 'minutes',
        duration: Math.floor(data.duration / 60),
        failCount: data.failCount,
        passCount: data.passCount,
        skipCount: data.skipCount
      }

      resolve(result)
    }
    else {
      reject(error)
    }
  })
})
