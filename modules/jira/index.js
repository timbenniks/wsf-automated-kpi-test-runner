const JiraApi = require('jira-client')

const jira = new JiraApi({
  protocol: 'https',
  host: process.env.JIRA_URL,
  username: process.env.JIRA_USERNAME,
  password: process.env.JIRA_PASSWORD,
  apiVersion: '2',
  strictSSL: true
})

module.exports = query => new Promise((resolve, reject) => {
  jira.searchJira(query)
    .then(data => {
      const result = {
        total: data.total,
        issues: data.issues.map((issue) => ({
          key: issue.key,
          labels: issue.fields.labels,
          created: issue.fields.created,
          assignee: {
            key: issue.fields.assignee.key,
            name: issue.fields.assignee.displayName,
            avatar: issue.fields.assignee.avatarUrls['48x48']
          },
          reporter: {
            key: issue.fields.reporter.key,
            name: issue.fields.reporter.displayName,
            avatar: issue.fields.reporter.avatarUrls['48x48']
          },
          project: {
            key: issue.fields.project.key,
            name: issue.fields.project.name,
          },
          description: issue.fields.description,
          summary: issue.fields.summary,
          fixVersions: issue.fields.fixVersions.map((fixVersion) => ({
            name: fixVersion.name,
            releaseDate: fixVersion.releaseDate
          })),
          priority: {
            icon: issue.fields.priority.iconUrl,
            name: issue.fields.priority.name
          },
          versions: issue.fields.versions.map((version) => ({
            name: version.name,
            description: version.description,
            releaseDate: version.releaseDate
          })),
          status: {
            name: issue.fields.status.name,
            description: issue.fields.status.description,
            iconUrl: issue.fields.status.iconUrl
          },
          creator: {
            key: issue.fields.creator.key,
            name: issue.fields.creator.displayName,
            avatar: issue.fields.creator.avatarUrls['48x48']
          }
        }))
      }

      result.assignees = result.issues.map((issue) => issue.assignee.key)
      result.statuses = result.issues.map((issue) => issue.status.name)
      result.priorities = result.issues.map((issue) => issue.priority.name)
      result.reporters = result.issues.map((issue) => issue.reporter.key)

      resolve(result)
    })
    .catch(reject)
})
