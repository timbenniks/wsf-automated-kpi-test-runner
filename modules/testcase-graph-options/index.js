const dateFormat = require('dateformat')

const graphTypePerKPI = kpi => {
  let type
  switch (kpi) {
    case 'wp6':
      type = 'bar'
      break
    case 'wp1':
    case 'wp4':
    default:
      type = 'line'
      break
  }

  return type
}

const tickValuePerKPI = (kpi, value) => {
  let tick
  switch (kpi) {
    case 'wp1':
    case 'wp4':
      tick = `${value}ms`
      break
    case 'wp6':
    case 'wp7':
    default:
      tick = value
      break
  }

  return tick
}

const datasetPerKPI = (kpi, results) => {
  const datasets = []

  if (kpi === 'wp1') {
    datasets.push({
      label: 'Average response time',
      backgroundColor: '#eee',
      borderColor: '#eee',
      borderWidth: 3,
      data: results.map(result => result.data.responseTime.avg),
      fill: false
    })

    datasets.push({
      label: 'Error percentage',
      backgroundColor: '#aaa',
      borderColor: '#aaa',
      borderWidth: 3,
      data: results.map(result => result.data.error.percent),
      fill: false
    })

    datasets.push({
      label: 'KPI maximum',
      backgroundColor: 'green',
      borderColor: 'green',
      borderWidth: 3,
      data: results.map(() => 3000),
      fill: false
    })
  }

  if (kpi === 'wp4') {
    datasets.push({
      label: 'Webpage Response Time',
      backgroundColor: '#eee',
      borderColor: '#eee',
      borderWidth: 3,
      data: results.map(result => result.data.summary[0].result),
      fill: false
    })

    datasets.push({
      label: 'KPI goal',
      backgroundColor: 'green',
      borderColor: 'green',
      borderWidth: 3,
      data: results.map(() => 3000),
      fill: false
    })

    datasets.push({
      label: 'KPI maximum',
      backgroundColor: 'red',
      borderColor: 'red',
      borderWidth: 3,
      data: results.map(() => 4000),
      fill: false
    })
  }

  if (kpi === 'wp6') {
    datasets.push({
      label: 'Amount of A11y failing',
      backgroundColor: 'rgba(255, 0, 0, 0.6)',
      borderColor: 'rgba(255, 0, 0, 0.6)',
      data: results.map((result) => result.data.filter(rule => rule.result === 'FAIL').length),
      fill: false
    })
  }

  return datasets
}

module.exports = (testcase, results) => {
  results.sort((a, b) => +new Date(a.date) - +new Date(b.date))

  const graphOptions = {
    type: graphTypePerKPI(testcase.kpi),
    data: {
      labels: results.map(result => dateFormat(new Date(result.date), 'mmm d HH:MM')),
      datasets: datasetPerKPI(testcase.kpi, results),
      options: {
        responsive: true,
        scales: {
          xAxes: [{
            type: 'time',
            stacked: true,
            scaleLabel: { display: false },
            ticks: {
              autoSkip: true
            }
          }],
          yAxes: [{
            display: true,
            scaleLabel: { display: false },
            ticks: {
              callback(value) {
                return tickValuePerKPI(testcase.kpi, value)
              }
            }
          }]
        }
      }
    }
  }

  return graphOptions
}
