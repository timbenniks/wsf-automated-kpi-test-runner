# wsf-automated-kpi-test-runner
> Automated KPI test runner for WSF websites.

## Global prerequisites:
* Nodemon (npm i -g nodemon)
* Foreman (npm i -g foreman)

## Development
`$ npm install`

### run server
`$ npm run be`

### run FE watcher
`$ npm run fe`

### run FE build (no watcher)
`$ npm run build-fe`
