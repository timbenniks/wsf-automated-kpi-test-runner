/* globals $ */
import Widget from './scripts/widget'
import fscreen from 'fscreen'
import Chart from 'chart.js'

$(() => {
  $('table').DataTable({
    searching: false,
    ordering: true,
    bFilter: false,
    bLengthChange: false,
    info: false,
    order: [[0, 'desc']],
    language: {
      search: '',
      searchPlaceholder: 'Filter records'
    }
  })

  // $('#addwidget').chosen()

  // const socket = io.connect()
  // socket.emit('ready')
  // socket.on('talk', (data) => {
  //   console.log(data.message)
  // })

  $('#run').on('click', () => {
    $('.loading-modal').css('display', 'flex')
  })

  if ($('.new-testcase').length) {
    const kpiSelect = $('#kpi')
    const kpiFormFieldHolders = $('.form-fields-for-testcase')

    const showHidePanels = (select) => {
      kpiFormFieldHolders.removeClass('form-fields-shown')
      if (select.value === 'wp1') {
        $('[data-testcase="wp1"]').addClass('form-fields-shown')
      }
      else if (select.value === 'sitecore_cache') {
        $('[data-testcase="sitecore_cache"]').addClass('form-fields-shown')
      }
      else {
        $('[data-testcase="others"]').addClass('form-fields-shown')
      }
    }

    showHidePanels(kpiSelect[0])

    kpiSelect.on('change', function () {
      showHidePanels(this)
    })
  }

  if ($('h1.edit-testcase').length && window.graphOptions) {
    Chart.defaults.global.defaultFontColor = '#eee'
    window.kpichart = new Chart($('.testcase-graph'), window.graphOptions)
  }

  // if ($('.home-results-graph').length) {
  //   window.kpichart = new Chart($('.home-results-graph'), window.homeResultsGraphOptions)
  // }

  if ($('h1.new-widget-type').length) {
    const chartTypeSelect = $('#charttype')
    const chartFieldsSelect = $('#chartfields')

    chartTypeSelect.on('change', function () {
      if (this.value === 'number') {
        chartFieldsSelect.val('tickets')
      }
      else {
        chartFieldsSelect.val('assignees')
      }
    })
  }

  if ($('h1.edit-dashboard').length) {
    const addPageButton = $('.dashboard-add-page')
    const pagesInput = $('#pages-field')
    const pagesInputVal = pagesInput.val()
    addPageButton.on('click', (event) => {
      event.preventDefault()
      pagesInput.val(Number(pagesInputVal) + 1)
      document.forms[0].submit()
    })
  }

  if ($('h1.dashboard__title').length) {
    // widgets
    const widgets = []
    $('.widget__item').each(function () {
      widgets.push(new Widget(this))
    })

    // dashboard pages
    const pageSlider = $('.widget__inner')
    const pages = $('.widget__list').length
    let currentPage = 0
    const timing = 20000
    const oLoader = document.querySelector('.loader')

    if (pages > 1) {
      setInterval(() => {
        if (currentPage >= pages - 1) {
          currentPage = 0
        } else {
          currentPage++
        }

        pageSlider.css({ transform: `translateX(-${currentPage * 100}vw)` })
      }, timing)
      oLoader.style.animationDuration = `${timing}ms`
    }

    // fullscreen
    if (fscreen.fullscreenEnabled) {
      $('#gofullscreen').show()

      fscreen.addEventListener('fullscreenchange', () => {
        if (fscreen.fullscreenElement !== null) {
          $('#gofullscreen').hide()
          $('#exitfullscreen').show()
          $('.page-dashboard').css({ position: 'absolute', width: '100%', height: '100%' })
        }
        else {
          $('#exitfullscreen').hide()
          $('#gofullscreen').show()
          $('.page-dashboard').css({ position: 'static', width: 'auto', height: 'auto' })
        }
      }, false)
    }

    $('#gofullscreen').on('click', () => {
      fscreen.requestFullscreen($('.dashboard')[0])
    })

    $('#exitfullscreen').on('click', () => {
      fscreen.exitFullscreen()
    })
  }
})
