/* globals $ */
export default class kpiPassFailWidget {
  constructor() {
    this.widgetData = {}
    this.element = null
    this.data = {}
  }

  setData(widgetData) {
    this.widgetData = widgetData
    this.node = this.widgetData.node
    this.element = this.widgetData.element
    this.data = this.widgetData.data
    this.statusClasses = this.widgetData.statusClasses

    if (this.data.status === 'PASS') {
      this.status = this.statusClasses[0]
    }

    if (this.data.status === 'WARNING') {
      this.status = this.statusClasses[1]
    }

    if (this.data.status === 'FAIL') {
      this.status = this.statusClasses[2]
    }
  }

  render() {
    $(this.node).find('.widget__content')[0].classList.add(this.status)

    const tmpl = `
      <p class="widget-outcome">${this.data.kpi}</p>
      <p class="widget-additional">${this.data.status} &mdash; ${this.data.additionalInfo}</p>
    `
    $(this.element).html(tmpl)
  }
}
