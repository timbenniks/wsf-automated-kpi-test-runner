/* globals $ */
export default class NumberWidget {
  constructor() {
    this.widgetData = {}
    this.element = null
    this.data = {}
  }

  setData(widgetData) {
    this.widgetData = widgetData
    this.element = this.widgetData.element
    this.data = this.widgetData.data.data
  }

  render() {
    $(this.element).html(this.data.total)
  }
}
