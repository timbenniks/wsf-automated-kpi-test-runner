/* globals $ */
import NumberWidget from './numberWidget'
import PieWidget from './pieWidget'
import BooleanWidget from './booleanWidget'
import KpiPassFailWidget from './kpiPassFailWidget'
import BlazeMeterWidget from './blazeMeterWidget'
import BurndownWidget from './burndownWidget'

// go here for colours: http://tools.medialab.sciences-po.fr/iwanthue/
const baseColors = ['#e47171', '#eee', '#888', '#880f0f', '#56A1D7', '#3a718b', '#002a33', '#4c707b', '#002a33',
  '#3f646e', '#002a33', '#245e78', '#002a33', '#074d65', '#002a33', '#2d525c', '#002a33', '#1b404a', '#002a33', '#002a33']

// const baseColors = ['#E08283', '#F1A9A0', '#AEA8D3', '#96281B', 'green', 'lime', 'black', '#999', '#ddd'];

const statusClasses = ['widget--pass', 'widget--warning', 'widget--fail']

export default class Widget {
  constructor(node) {
    this.node = node
    this.element = node.querySelector('.widget__data-content')
    this.widgetId = node.id
    this.widgetInstance = null
    this.instantiated = false
    this.data = null
    this.colors = baseColors
    this.statusClasses = statusClasses
    this.getData()
  }

  getData() {
    this.setLoading(true)
    $.get(`/api/widget/${this.widgetId}`)
      .done((data) => {
        this.data = data
        this.setLoading(false)
        this.instantiate()
        this.widgetInstance.setData(this)
        this.widgetInstance.render()
        console.log(`%cReceived %cdata for %c[${this.data.title}]`, 'font-weight: bold;', 'color: #666;', 'color: #666; background: #eee')

        console.log(`%cNo refreshrate %cfor %c[${this.data.title}]`, 'font-weight: bold;', 'color: #666;', 'color: #666; background: #eee')

        if (this.node.dataset.refreshrate !== 'never') {
          setTimeout(() => {
            console.log(`%cRequesting %cdata for %c[${this.data.title}]`, 'font-weight: bold;', 'color: #666;', 'color: #666; background: #eee')
            this.getData()
          }, this.node.dataset.refreshrate * 1000)
        }
      })
      .fail((error) => {
        console.error('fail', error)
        setTimeout(() => { this.getData() }, 5000)
      })
  }

  instantiate() {
    if (this.instantiated) {
      return
    }

    this.instantiated = true

    switch (this.data.charttype) {
      case 'number':
        this.widgetInstance = new NumberWidget()
        break
      case 'pie':
        this.widgetInstance = new PieWidget()
        break
      case 'boolean':
        this.widgetInstance = new BooleanWidget()
        break
      case 'kpi-pass-fail':
        this.widgetInstance = new KpiPassFailWidget()
        break
      case 'blazemeter':
        this.widgetInstance = new BlazeMeterWidget()
        break
      case 'burndown':
        this.widgetInstance = new BurndownWidget()
        break

      default:
        break
    }
  }

  setLoading(state) {
    if (state) {
      this.node.classList.add('widget-loading')
    }
    else {
      this.node.classList.remove('widget-loading')
    }
  }
}
