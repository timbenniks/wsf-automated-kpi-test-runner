import Chart from 'chart.js'

export default class PieWidget {
  constructor() {
    this.widgetData = {}
    this.element = null
    this.data = {}
    this.chart = null
  }

  setData(widgetData) {
    this.widgetData = widgetData
    this.element = this.widgetData.element.getContext('2d')
    this.data = this.widgetData.data.data

    if (this.widgetData.data.type === 'selenium') {
      this.unique = ['failed', 'passed', 'skipped']
      this.amount = [this.data.failCount, this.data.passCount, this.data.skipCount]
      this.colors = [this.widgetData.colors[0], this.widgetData.colors[1], this.widgetData.colors[2]]
    }
    else {
      this.field = this.widgetData.data.chartfields
      this.unique = this.data[this.field].filter((item, index, array) => array.indexOf(item) === index)
      this.amount = this.unique.map(field => this.countInArray(this.data[this.field], field))
      this.colors = this.unique.map((field, index) => this.widgetData.colors[index])
    }

    this.setDataset()
  }

  countInArray(array, value) {
    return array.reduce((n, x) => n + (x === value), 0)
  }

  setDataset() {
    this.dataset = {
      datasets: [{
        backgroundColor: this.colors,
        data: this.amount
      }],
      labels: this.unique
    }
  }

  getColor() {
    const oDash = document.querySelector('.dashboard')
    const oDashColor = window.getComputedStyle(oDash, null).getPropertyValue('background-color')

    let sColor

    if (oDashColor === 'rgb(34, 34, 34)') {
      sColor = '#fff'
    }
    else {
      sColor = '#222'
    }

    return sColor
  }

  render() {
    const textColor = this.getColor()

    if (this.instantiated) {
      this.setDataset()
      this.chart.update()
    }
    else {
      this.instantiated = true
      this.chart = new Chart(this.element, {
        type: 'doughnut',
        data: this.dataset,
        options: {
          responsive: true,
          layout: {
            padding: {
              left: 0,
              right: 0,
              top: 0,
              bottom: 0
            }
          },
          title: {
            display: this.widgetData.data.pieAditionalInfo,
            position: 'bottom',
            text: this.widgetData.data.pieAditionalInfo,
            fontColor: textColor,
            fontSize: 25,
            lineHeight: 1.5,
            padding: 20,
            fontFamily: "'Open Sans', sans-serif"
          },
          legend: {
            display: true,
            position: 'bottom',
            fullWidth: true,

            labels: {
              fontColor: textColor,
              boxWidth: 20,
              fontSize: 20,
              padding: 40,
              fontFamily: "'Open Sans', sans-serif"
            }
          }
        }
      })
    }
  }
}
