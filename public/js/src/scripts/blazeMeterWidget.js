/* globals $ */
export default class blazeMeterWidget {
  constructor() {
    this.widgetData = {}
    this.element = null
    this.data = {}
  }

  setData(widgetData) {
    this.widgetData = widgetData
    this.node = this.widgetData.node
    this.element = this.widgetData.element
    this.data = this.widgetData.data
    this.booleanCls = this.widgetData.booleanCls
  }

  render() {
    $(this.node).find('.widget__content')[0].classList.add(this.booleanCls[(this.data.status === 'PASS') ? 0 : 1])

    const tmpl = `
      <p class="widget-outcome">${this.data.kpi}</p>
      <p class="widget-additional">${this.data.status} &mdash; ${this.data.additionalInfo}</p>
    `
    $(this.element).html(tmpl)
  }
}
