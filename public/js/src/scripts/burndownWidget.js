import Chart from 'chart.js'
import dateFormat from 'dateformat'

export default class BurndownWidget {
  constructor() {
    this.widgetData = {}
    this.element = null
    this.data = {}
    this.chart = null
  }

  setData(widgetData) {
    this.widgetData = widgetData
    this.element = this.widgetData.element.getContext('2d')
    this.data = this.widgetData.data.data

    this.dates = this.data.changes.map((datapoint) => datapoint.date)
    this.sprintStartIndex = this.dates.findIndex(date => date > this.data.startTime)
    this.sprintEndIndex = this.dates.findIndex(date => date > this.data.endTime)

    if (this.data.completeTime) {
      this.completeEndIndex = this.dates.findIndex(date => date > this.data.completeTime)
    }

    console.log('datapoints', this.dates.length)
    console.log('datapoint start', new Date(Number(this.dates[0])))
    console.log('sprint official start', new Date(Number(this.data.startTime)), this.sprintStartIndex)
    console.log('sprint official end', new Date(Number(this.data.endTime)), this.sprintEndIndex)
    console.log('complete', new Date(Number(this.data.completeTime)), this.completeEndIndex)

    this.changes = this.data.changes.slice(this.sprintStartIndex)
    console.log('changes', 'remove sprintStartDate from changes', this.changes.length)

    this.endIndexAfterOfficialStart = this.changes.findIndex(change => change.date > this.data.endTime)
    console.log('endIndexAfterOfficialStart', this.endIndexAfterOfficialStart)

    this.changes = this.data.changes.splice(this.completeEndIndex)
    console.log('changes dates', new Date(Number(this.changes[0].date)), new Date(Number(this.changes[this.changes.length - 1].date)))
    console.log('baseline start and end', new Date(Number(this.changes[0].date)), new Date(Number(this.changes[this.endIndexAfterOfficialStart].date)))

    this.labels = this.changes.map((datapoint) => datapoint.date)

    let timeSpendTotal = 0
    this.timeSpentData = this.changes.map((datapoint) => {
      timeSpendTotal += datapoint.timeSpent
      return (timeSpendTotal / 60 / 60).toFixed(0)
    })

    let timeEstimatedTotal = 0
    let timeEstimatedTotalForBaseLine = 0
    this.changes.forEach((datapoint) => {
      timeEstimatedTotal += datapoint.oldEstimate
      timeEstimatedTotalForBaseLine += datapoint.oldEstimate
    })

    this.remainingValuesData = this.changes.map((datapoint) => {
      timeEstimatedTotal -= (datapoint.oldEstimate - datapoint.newEstimate)
      return (timeEstimatedTotal / 60 / 60).toFixed(0)
    })

    this.baselineData = []
    let baselineCurrentAmount = timeEstimatedTotalForBaseLine
    for (let index = 0; index < this.endIndexAfterOfficialStart; index++) {
      baselineCurrentAmount -= (timeEstimatedTotalForBaseLine / this.endIndexAfterOfficialStart)
      this.baselineData[index] = (baselineCurrentAmount / 60 / 60)
    }
  }

  render() {
    this.chart = new Chart(this.element, {
      type: 'line',
      data: {
        labels: this.labels,
        datasets: [{
          label: 'Time spent',
          backgroundColor: 'green',
          borderColor: 'green',
          borderWidth: 2,
          data: this.timeSpentData,
          fill: false,
          lineTension: 0
        },
        {
          label: 'Remaining values',
          backgroundColor: 'red',
          borderColor: 'red',
          borderWidth: 2,
          data: this.remainingValuesData,
          fill: false,
          lineTension: 0
        },
        {
          label: 'baseline',
          backgroundColor: '#aaa',
          borderColor: '#aaa',
          borderWidth: 2,
          data: this.baselineData,
          fill: false,
          lineTension: 0
        }]
      },
      options: {
        elements: {
          point: {
            radius: 0
          }
        },
        layout: {
          padding: {
            left: 0,
            right: 0,
            top: 0,
            bottom: 0
          }
        },
        responsive: true,
        title: {
          display: false
        },
        tooltips: {
          enabled: false
        },
        scales: {
          xAxes: [{
            display: true,
            scaleLabel: {
              display: false
            },
            ticks: {
              autoSkip: true,
              maxTicksLimit: 10,
              callback(value) {
                return dateFormat(new Date(Number(value)), 'mmm d')
              }
            }
          }],
          yAxes: [{
            display: true,
            scaleLabel: {
              display: false
            },
            ticks: {
              autoSkip: true,
              maxTicksLimit: 10,
              callback(value) {
                return `${value}h`
              }
            }
          }]
        }
      }
    })
  }
}
