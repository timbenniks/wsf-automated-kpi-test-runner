/* globals $ */
export default class BooleanWidget {
  constructor() {
    this.widgetData = {}
    this.element = null
    this.data = {}
  }

  setData(widgetData) {
    this.widgetData = widgetData
    this.node = this.widgetData.node
    this.element = this.widgetData.element
    this.data = this.widgetData.data
    this.statusClasses = this.widgetData.statusClasses

    if (this.data.booleanStatus) {
      this.status = this.statusClasses[0]
    }

    if (!this.data.booleanStatus) {
      this.status = this.statusClasses[2]
    }
  }

  render() {
    $(this.node).find('.widget__content')[0].classList.add(this.status)

    const tmpl = `
      <p class="widget-outcome">${this.data.booleanStatus ? 'Yes' : 'No'}</p>
      <p class="widget-additional">${this.data.additionalInfo}</p>
    `
    $(this.element).html(tmpl)
  }
}
