const gulp = require('gulp')
const sass = require('gulp-sass')
const browserify = require('browserify')
const babelify = require('babelify')
const watchify = require('watchify')
const uglifyify = require('uglifyify')
const source = require('vinyl-source-stream')

const paths = {
  sass: {
    src: './public/css/src/**/*.scss',
    dest: './public/css'
  },
  scripts: {
    src: './public/js/src/**/*.js',
    dest: './public/js',
    opts: Object.assign(watchify.args, { entries: ['./public/js/src/app.js'] })
  }
}

const scriptWatcher = watchify(browserify(paths.scripts.opts))
scriptWatcher.transform(babelify)

if (process.env.NODE_ENV === 'production') {
  scriptWatcher.transform(uglifyify, { global: true })
}

scriptWatcher.on('update', bundle) // on any dep update, runs the bundler
scriptWatcher.on('log', log => { console.log(log) }) // output build logs to terminal

function bundle() {
  return scriptWatcher.bundle()
    .on('error', error => { console.log('Browserify Error', error) })
    .pipe(source('bundle.js'))
    .pipe(gulp.dest(paths.scripts.dest))
}

gulp.task('sass', () => gulp.src(paths.sass.src)
  .pipe(sass().on('error', sass.logError))
  .pipe(gulp.dest(paths.sass.dest)))

gulp.task('scripts', bundle)

gulp.task('message', () => {
  console.log('Starting FE build')
})

gulp.task('watch:styles', () => {
  gulp.watch(paths.sass.src, gulp.series('sass'))
})

gulp.task('watch:scripts', () => {
  gulp.watch(paths.scripts.src, gulp.series('scripts'))
})

gulp.task('watch', gulp.series(
  'sass',
  'scripts',
  gulp.parallel('watch:styles', 'watch:scripts')
))

gulp.task('fe', gulp.series('sass', 'scripts'))

gulp.task('default', gulp.series(gulp.parallel('message', 'watch')))
