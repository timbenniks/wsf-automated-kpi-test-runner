const router = require('express').Router()
const dateFormat = require('dateformat')
const uuidv1 = require('uuid/v1')
const wp1 = require('../runners/wp1')
const wp4 = require('../runners/wp4')
const wp6 = require('../runners/wp6')
const wp7 = require('../runners/wp7')
const wp8 = require('../runners/wp8')
// const sitecore_cache = require('../runners/sitecore_cache')
const confluence = require('../modules/confluence')

const removeLastCharIf = (str, toRemove = '&') => {
  if (str.substring(str.length - 1) === toRemove) {
    str = str.substring(0, str.length - 1)
  }

  return str
}

const getSelectedQSForFilter = (key, value, qs) => {
  let selected = false

  if (key in qs) {
    for (const q in qs) {
      if ({}.hasOwnProperty.call(qs, q)) {
        if (qs[q] === value) {
          selected = true
        }
      }
    }
  }

  return selected
}

const createQSForFilter = (key, value, qs) => {
  let qsToReturn = ''
  let baseQs = '?'

  if (!Object.keys(qs).length) {
    qsToReturn = `?${key}=${value}`
  }
  else if (key in qs) {
    for (const q in qs) {
      if ({}.hasOwnProperty.call(qs, q)) {
        if (q === key && qs[q] !== value) {
          baseQs += `${q}=${value}&`
        }
        else if (q === key && qs[q] === value) {
          baseQs += ''
        }
        else {
          baseQs += `${q}=${qs[q]}&`
        }
      }
    }
    qsToReturn = `${baseQs}`
  }
  else {
    for (const q in qs) {
      if ({}.hasOwnProperty.call(qs, q)) {
        baseQs += `${q}=${qs[q]}&`
      }
    }
    qsToReturn = `${baseQs}${key}=${value}`
  }

  return removeLastCharIf(qsToReturn, '&')
}

router.get('/', (req, res) => {
  const collection = req.database.getCollection('results').chain().simplesort('id', true).data()

  const results = []
  let kpis = []
  let statuses = []
  let projects = []
  let envs = []

  collection.forEach(item => {
    kpis.push(item.kpi)
    statuses.push(item.status)
    projects.push(item.project)
    envs.push(item.environment)
  })

  kpis = kpis.filter((item, index, array) => array.indexOf(item) === index)
  statuses = statuses.filter((item, index, array) => array.indexOf(item) === index)
  projects = projects.filter((item, index, array) => array.indexOf(item) === index)
  envs = envs.filter((item, index, array) => array.indexOf(item) === index)

  kpis = kpis.map((filter) => ({
    filter,
    url: createQSForFilter('kpi', filter, req.query),
    selected: getSelectedQSForFilter('kpi', filter, req.query)
  }))

  statuses = statuses.map((filter) => ({
    filter,
    url: createQSForFilter('status', filter, req.query),
    selected: getSelectedQSForFilter('status', filter, req.query)
  }))

  projects = projects.map((filter) => ({
    filter,
    url: createQSForFilter('project', filter, req.query),
    selected: getSelectedQSForFilter('project', filter, req.query)
  }))

  envs = envs.map((filter) => ({
    filter,
    url: createQSForFilter('environment', filter, req.query),
    selected: getSelectedQSForFilter('environment', filter, req.query)
  }))

  const combinedResultFilterObject = {}
  if (req.query.kpi) { combinedResultFilterObject.kpi = req.query.kpi }
  if (req.query.status) { combinedResultFilterObject.status = req.query.status }
  if (req.query.project) { combinedResultFilterObject.project = req.query.project }
  if (req.query.env) { combinedResultFilterObject.env = req.query.env }

  const filteredCollection = req.database.getCollection('results')
    .chain()
    .find(combinedResultFilterObject)
    .simplesort('id', true)
    .data()

  filteredCollection.forEach(item => {
    item.dateUnix = new Date(item.date).getTime()
    item.date = dateFormat(new Date(item.date))
    results.push(item)
  })

  res.render('results/results', {
    title: 'Results',
    active: 'results',
    results,
    kpis,
    statuses,
    projects,
    envs
  })
})

router.get('/delete/:id', (req, res) => {
  if (!req.params.id || req.params.id === '') {
    res.status(404)
    res.render('404', { active: '404' })
  }
  else {
    const result = req.database.getCollection('results').find({ id: req.params.id })

    if (result.length === 0) {
      res.status(404)
      res.render('404', { active: '404' })
    }

    res.render('delete_item', {
      title: 'Delete result set',
      type: 'results',
      active: 'results',
      item: result[0]
    })
  }
})

router.get('/export/:id', (req, res) => {
  if (!req.params.id || req.params.id === '') {
    res.status(404)
    res.render('404', { active: '404' })
  }
  else {
    const result = req.database.getCollection('results').find({ id: req.params.id })

    if (result.length === 0) {
      res.status(404)
      res.render('404', { active: '404' })
    }

    result[0].resultString = JSON.stringify(result[0].data, null, 2)

    res.render('results/export', {
      title: `export ${result[0].title}`,
      active: 'results',
      item: result[0]
    })
  }
})

router.post('/export', (req, res) => {
  confluence(req.body.space, req.body.parentid, req.body.title, req.body.content)
    .then(res.redirect('/results/'))
    .catch(res.redirect('/results/'))
})

router.get('/see/:id', (req, res) => {
  if (!req.params.id || req.params.id === '') {
    res.status(404)
    res.render('404', { active: '404' })
  }
  else {
    const result = req.database.getCollection('results').find({ id: req.params.id })
    const testcase = req.database.getCollection('testcases').find({ id: result[0].testcase_id })

    if (result.length === 0) {
      res.status(404)
      res.render('404', { active: '404' })
    }

    res.render('results/result', {
      title: 'result',
      active: 'results',
      result: result[0],
      resultShow: JSON.stringify(result[0], null, 2),
      testcase: testcase[0]
    })
  }
})

router.post('/delete/', (req, res) => {
  req.database.getCollection('results').chain().find({ id: req.body.id }).remove()
  res.redirect('/results/')
})

router.get('/run/:id', (req, res) => {
  req.connection.setTimeout(60 * 10 * 1000)

  const testcase = req.database.getCollection('testcases').chain().find({ id: req.params.id }).data()[0]
  const results = req.database.getCollection('results')
  const settings = req.database.getCollection('settings')

  const result = {
    id: uuidv1(),
    testcase_id: testcase.id,
    kpi: testcase.kpi,
    project: testcase.project,
    url: testcase.url,
    frequency: testcase.frequency,
    environment: testcase.environment,
    title: testcase.title,
    date: new Date(),
    blazemeter_testid: testcase.blazemeter_testid || '',
    sitecore_stats_url: testcase.sitecore_stats_url || ''
  }

  const run = (kpi, datapoint) => {
    let runner
    if (kpi === 'wp1') { runner = wp1(datapoint) }
    // if (kpi === 'sitecore_cache') { runner = sitecore_cache(datapoint) }

    if (kpi === 'wp4') { runner = wp4(datapoint, settings) }
    if (kpi === 'wp6') { runner = wp6(datapoint) }
    if (kpi === 'wp7') { runner = wp7(datapoint) }
    if (kpi === 'wp8') { runner = wp8(datapoint) }

    runner
      .then(response => {
        result.status = response.status
        result.data = response.data
        result.readableOutcome = response.readableOutcome
        results.insert(result)
        res.redirect(`/results/see/${result.id}`)
      })
      .catch(error => res.json({ error }))
  }


  let datapoint = testcase.url
  if (testcase.kpi === 'wp1') {
    datapoint = testcase.blazemeter_testid
  }

  if (testcase.kpi === 'sitecore_cache') {
    datapoint = testcase.sitecore_stats_url
  }

  run(testcase.kpi, datapoint)
})

module.exports = router
