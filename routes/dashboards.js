const router = require('express').Router()
const uuidv1 = require('uuid/v1')

router.get('/', (req, res) => {
  const dashboards = req.database.getCollection('dashboards').chain().simplesort('date', true).data()

  res.render('dashboards/dashboards', {
    title: 'Dashboards',
    active: 'dashboards',
    dashboards
  })
})

router.get('/new', (req, res) => {
  res.render('dashboards/new_dashboard', {
    title: 'New Dashboard',
    active: 'dashboards'
  })
})

router.post('/new', (req, res) => {
  const toSave = {
    id: uuidv1(),
    title: req.body.title,
    description: req.body.description,
    pages: req.body.pages,
    logourl: req.body.logourl,
    color: req.body.color,
    layout: [req.body.layout],
    widgets: []
  }

  const collection = req.database.getCollection('dashboards')
  collection.insert(toSave)
  res.redirect(`/dashboards/edit/${toSave.id}`)
})

router.get('/edit/:id', (req, res) => {
  if (!req.params.id || req.params.id === '') {
    res.status(404)
    res.render('404', { active: '404' })
  }
  else {
    const dashboard = req.database.getCollection('dashboards').find({ id: req.params.id })
    const widgets = req.database.getCollection('widgets').chain().simplesort('id', true).data()

    if (dashboard.length === 0) {
      res.status(404)
      res.render('404', { active: '404' })
    }

    const returnWidgetForId = id => req.database.getCollection('widgets').findOne({ id })

    const positions = Array(Number(dashboard[0].widgets.length)).fill().map((e, i) => i + 1)
    const pagesAsArray = Array(Number(dashboard[0].pages)).fill().map((e, i) => i + 1)

    const widgetsForDash = []
    dashboard[0].widgets.forEach((widget) => {
      widgetsForDash.push({
        id: widget.id,
        position: Number(widget.position),
        page: Number(widget.page),
        refreshrate: widget.refreshrate,
        ...returnWidgetForId(widget.id),
        pagesAsArray,
        positions
      })
    })
    widgetsForDash.sort((a, b) => a.position - b.position)

    let pagesAndWidgets = []
    pagesAndWidgets = pagesAsArray.map((pageId, index) => ({
      index: index + 1,
      widgets: widgetsForDash.filter(w => w.page === pageId),
      hasWidgets: !!(widgetsForDash.filter(w => w.page === pageId).length),
      layout: dashboard[0].layout[index]
    }))

    res.render('dashboards/edit_dashboard', {
      title: `Edit Dashboard: ${dashboard[0].title}`,
      active: 'dashboards',
      dashboard: dashboard[0],
      widgets,
      widgetsForDash,
      pagesAndWidgets,
      hasWidgets: dashboard[0].widgets && dashboard[0].widgets.length > 0
    })
  }
})

router.get('/run/:id', (req, res) => {
  if (!req.params.id || req.params.id === '') {
    res.status(404)
    res.render('404', { active: '404' })
  }
  else {
    const dashboard = req.database.getCollection('dashboards').find({ id: req.params.id })
    const widgetCollection = req.database.getCollection('widgets')

    const returnWidgetDataForId = id => widgetCollection.findOne({ id })

    const lightColor = hc => {
      const [r, g, b] = [0, 2, 4].map(p => parseInt(hc.substr(p, 2), 16))
      return ((r * 299) + (g * 587) + (b * 114)) / 1000 >= 128
    }

    const widgets = dashboard[0].widgets.map((widget) => ({
      id: widget.id,
      page: Number(widget.page),
      position: Number(widget.position),
      refreshrate: widget.refreshrate,
      canvas: returnWidgetDataForId(widget.id).charttype === 'pie' || returnWidgetDataForId(widget.id).charttype === 'burndown',
      ...returnWidgetDataForId(widget.id)
    })).sort((a, b) => a.position - b.position)

    const pagesAsArray = Array(Number(dashboard[0].pages)).fill().map((e, i) => i + 1)

    const pages = pagesAsArray.map((pageId, index) => ({
      widgets: widgets.filter(w => w.page === pageId),
      layout: dashboard[0].layout[index],
    }))

    if (dashboard.length === 0) {
      res.status(404)
      res.render('404', { active: '404' })
    }

    const light = lightColor(dashboard[0].color.replace('#', ''))

    res.render('dashboards/dashboard', {
      title: `Dashboard: ${dashboard[0].title}`,
      layout: 'dashboard',
      active: 'dashboards',
      dashboard: dashboard[0],
      pages,
      light
    })
  }
})

router.post('/update', (req, res) => {
  const gatherWidgetInfo = (ids, positions, pages, refresrates) => ids.map((widget, index) => ({
    id: ids[index],
    position: positions[index],
    page: pages[index],
    refreshrate: refresrates[index]
  }))

  const removeFromArray = (array, element) => array.filter(e => e !== element)

  const toSave = {
    id: req.body.id,
    description: req.body.description,
    title: req.body.title,
    color: req.body.color,
    pages: req.body.pages,
    logourl: req.body.logourl,
    addWidgetId: req.body.addwidget,
    layout: (typeof req.body.layout === 'string') ? [req.body.layout] : req.body.layout
  }

  const collection = req.database.getCollection('dashboards')
  const dashboard = collection.find({ id: toSave.id })

  if (dashboard.length === 0) {
    res.status(404)
    res.render('404', { active: '404' })
  }

  if (req.body.widgetid) {
    let widgetid = req.body.widgetid // eslint-disable-line prefer-destructuring
    if (typeof req.body.widgetid === 'string') {
      widgetid = [req.body.widgetid]
    }

    let position = req.body.position // eslint-disable-line prefer-destructuring
    if (typeof req.body.position === 'string') {
      position = [req.body.position]
    }

    let page = req.body.page // eslint-disable-line prefer-destructuring
    if (typeof req.body.page === 'string') {
      page = [req.body.page]
    }

    let refreshrate = req.body.refreshrate // eslint-disable-line prefer-destructuring
    if (typeof req.body.refreshrate === 'string') {
      refreshrate = [req.body.refreshrate]
    }

    toSave.widgets = gatherWidgetInfo(widgetid, position, page, refreshrate)
    dashboard[0].widgets = toSave.widgets
  }

  if (toSave.addWidgetId !== 'choose') {
    const amountOfWidgets = dashboard[0].widgets.length || 0
    dashboard[0].widgets.push({
      id: toSave.addWidgetId, position: amountOfWidgets + 1, page: 1, refreshrate: 600
    })
  }

  if (req.body.removewidget) {
    const widgetToRemove = dashboard[0].widgets.find(widget => widget.id === req.body.removewidget)
    const widgets = removeFromArray(dashboard[0].widgets, widgetToRemove)

    dashboard[0].widgets = widgets
  }

  dashboard[0].title = toSave.title
  dashboard[0].description = toSave.description
  dashboard[0].color = toSave.color
  dashboard[0].logourl = toSave.logourl
  dashboard[0].pages = toSave.pages
  dashboard[0].layout = toSave.layout

  if (req.body.deletepage) {
    const widgets = dashboard[0].widgets.filter(widget => Number(widget.page) !== Number(req.body.deletepage))
    dashboard[0].widgets = widgets
    dashboard[0].pages--
  }

  if (dashboard[0].pages <= 0) {
    dashboard[0].pages = 1
  }

  collection.update(dashboard)

  res.redirect(`/dashboards/edit/${toSave.id}`)
})

router.get('/delete/:id', (req, res) => {
  if (!req.params.id || req.params.id === '') {
    res.status(404)
    res.render('404', { active: '404' })
  }
  else {
    const dashboard = req.database.getCollection('dashboards').find({ id: req.params.id })

    if (dashboard.length === 0) {
      res.status(404)
      res.render('404', { active: '404' })
    }

    res.render('delete_item', {
      title: `Delete dashboard: ${dashboard[0].title}`,
      type: 'dashboards',
      active: 'dashboards',
      item: dashboard[0]
    })
  }
})

router.post('/delete/', (req, res) => {
  req.database.getCollection('dashboards').chain().find({ id: req.body.id }).remove()
  res.redirect('/dashboards/')
})

module.exports = router
