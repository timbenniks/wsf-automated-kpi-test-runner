const router = require('express').Router()

router.get('/', (req, res) => {
  const settings = req.database.getCollection('settings').chain().data()

  const settingsForTmpl = []
  settings.forEach((setting) => {
    settingsForTmpl[setting.id] = setting.value
  })

  res.render('settings', {
    title: 'Settings',
    active: 'settings',
    settingsForTmpl
  })
})

router.post('/', (req, res) => {
  const collection = req.database.getCollection('settings')
  const settingsJiraUrl = collection.findOne({ id: 'settingsJiraUrl' })
  const settingsJiraUsername = collection.findOne({ id: 'settingsJiraUsername' })
  const settingsJiraPassword = collection.findOne({ id: 'settingsJiraPassword' })
  const settingsGoogleApikey = collection.findOne({ id: 'settingsGoogleApikey' })
  const catchpointClientId = collection.findOne({ id: 'catchpointClientId' })
  const catchpointClientSecret = collection.findOne({ id: 'catchpointClientSecret' })

  settingsJiraUrl.value = req.body.settingsJiraUrl
  settingsJiraUsername.value = req.body.settingsJiraUsername
  settingsJiraPassword.value = req.body.settingsJiraPassword
  settingsGoogleApikey.value = req.body.settingsGoogleApikey
  catchpointClientId.value = req.body.catchpointClientId
  catchpointClientSecret.value = req.body.catchpointClientSecret

  res.redirect('/settings/')
})
module.exports = router
