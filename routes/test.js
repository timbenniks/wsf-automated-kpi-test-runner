const router = require('express').Router()

router.get('/', (req, res) => {
  req.connection.setTimeout(60 * 10 * 1000)

  const result = req.database.getCollection('results')
    .chain()
    .find({
      testcase_id: 'ca2f42a0-ff9e-11e7-9a69-a9b69be779ec'
    })
    .simplesort('date', true)
    .data()

  res.json(result)
})

module.exports = router
