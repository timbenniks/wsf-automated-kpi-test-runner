const router = require('express').Router()
const jira = require('../modules/jira')
const blazeMeter = require('../modules/blazemeter')
const burndown = require('../modules/burndown')
const selenium = require('../modules/selenium')
const buildstatus = require('../modules/buildstatus')

router.get('/', (req, res) => {
  let results = {}
  let render
  let combiFilterObject = {}

  if (req.query.result_id) {
    results = req.database.getCollection('results').chain().find({ id: req.query.result_id }).data()[0]
    render = results
  }
  else if (req.query.testcase_id) {
    combiFilterObject = {
      testcase_id: req.query.testcase_id
    }

    if (req.query.date_start && !req.query.date_end) { combiFilterObject.date = { $gte: new Date(req.query.date_start) } }
    if (!req.query.date_start && req.query.date_end) { combiFilterObject.date = { $lte: new Date(req.query.date_end) } }
    if (req.query.date_start && req.query.date_end) { combiFilterObject.date = { $between: [new Date(req.query.date_start), new Date(req.query.date_end)] } }

    results = req.database.getCollection('results')
      .chain()
      .find(combiFilterObject)
      .limit(req.query.limit)
      .offset(req.query.offset)
      .simplesort('date', true)
      .data()

    render = results
  }
  else {
    combiFilterObject = {}
    if (req.query.kpi) { combiFilterObject.kpi = req.query.kpi }
    if (req.query.project) { combiFilterObject.project = req.query.project }
    if (req.query.environment) { combiFilterObject.environment = req.query.environment }
    if (req.query.date_start && !req.query.date_end) { combiFilterObject.date = { $gte: new Date(req.query.date_start) } }
    if (!req.query.date_start && req.query.date_end) { combiFilterObject.date = { $lte: new Date(req.query.date_end) } }
    if (req.query.date_start && req.query.date_end) { combiFilterObject.date = { $between: [new Date(req.query.date_start), new Date(req.query.date_end)] } }

    results = req.database.getCollection('results')
      .chain()
      .find(combiFilterObject)
      .limit(req.query.limit)
      .offset(req.query.offset)
      .simplesort('date', true)
      .data()

    render = results
  }

  res.json(render)
})

router.get('/all', (req, res) => {
  const results = req.database.getCollection('results')
    .chain()
    .simplesort('date', true)
    .limit(req.query.limit)
    .offset(req.query.offset)
    .data()

  res.json(results)
})

router.get('/help', (req, res) => {
  res.json({
    message: 'Welcome to the API, you can filter results by combining parameters.',
    filters: ['kpi', 'project', 'date_start', 'date_end', 'id', 'testcase_id', 'environment'],
    others: ['limit', 'offset'],
    simple_examples: [
      '/api/result/b969bc00-cee0-11e7-8606-a11a67e06b5e',
      '/api/?result_id=b969bc00-cee0-11e7-8606-a11a67e06b5e',
      '/api/?testcase_id=b969bc00-cee0-11e7-8606-a11a67e06b5e',
      '/api/?kpi=wp6',
      '/api/?environment=prd'
    ],
    complex_examples: [
      '/api/?kpi=wp6&project=Kerastase&environment=prd',
      '/api/?testcase_id=9195e1b0-cede-11e7-8804-85671cc2fdd8&limit=3',
      '/api/?kpi=wp6&offset=3',
      '/api/?project=kerastase&environment=prd&date_start=2017-11-21T17:23:43.680Z&date_end=2017-11-21T17:40:43.680Z'
    ]
  })
})

router.get('/result/:id', (req, res) => {
  const results = req.database.getCollection('results').chain().find({ id: req.params.id }).data()[0]
  res.json(results)
})

router.get('/widget/:id', (req, res) => {
  const widget = req.database.getCollection('widgets').chain().find({ id: req.params.id }).data()[0]

  if (widget.type === 'build-status') {
    buildstatus(widget.build_project, widget.build_job)
      .then((result) => {
        widget.additionalInfo = `Build ${result.release} ${(result.result === 'FAILURE') ? 'failed' : 'passed'} in ${(result.duration / 1000).toFixed(0)} seconds`
        widget.date = new Date()
        widget.data = result
        widget.kpi = 'CI Build'
        widget.status = (result.result === 'FAILURE') ? 'FAIL' : 'PASS'
        res.json(widget)
      })
      .catch((error) => {
        res.json(error)
      })
  }

  if (widget.type === 'selenium') {
    selenium(widget.selenium_project, widget.selenium_job)
      .then((result) => {
        widget.additionalInfo = `Selenium ran for ${result.duration} ${result.durationType}.<br/>${result.failCount} tests failed, ${result.passCount} tests passed and ${result.skipCount} tests were skipped.`
        widget.pieAditionalInfo = `${(result.failCount > 0) ? 'FAIL' : 'PASS'} — Selenium ran for ${result.duration} ${result.durationType}`
        widget.status = (result.failCount > 0) ? 'FAIL' : 'PASS'
        widget.date = new Date()
        widget.data = result

        if (widget.charttype === 'boolean') {
          widget.booleanStatus = !(result.failCount > 0)
        }

        res.json(widget)
      })
      .catch((error) => {
        res.json(error)
      })
  }

  if (widget.charttype === 'burndown') {
    burndown(widget.jira_board_id, widget.jira_sprint_id)
      .then((result) => {
        widget.additionalInfo = ''
        widget.date = new Date()
        widget.data = result
        res.json(widget)
      })
      .catch((error) => {
        res.json(error)
      })
  }

  if (widget.charttype === 'blazemeter') {
    blazeMeter(widget.blazemeter_testid)
      .then((result) => {
        widget.additionalInfo = `Errors: ${result.error.percent}% Avg reponse time: ${(result.responseTime.avg / 1000).toFixed(2)} (${(result.score).toFixed(2)}%)`
        widget.status = (result.score > 99) ? 'PASS' : 'FAIL'
        widget.score = result.score
        widget.kpi = 'WP1'
        widget.date = new Date()
        widget.data = result
        res.json(widget)
      })
      .catch((error) => {
        res.json(error)
      })
  }

  if (widget.dataset === 'internal-api' && widget.charttype !== 'blazemeter' && widget.charttype !== 'burndown' && widget.type !== 'selenium' && widget.type !== 'build-status') {
    const result = req.database.getCollection('results')
      .chain()
      .find({ testcase_id: widget.testcase })
      .simplesort('date', true)
      .limit(1)
      .data()

    if (result.length === 0) {
      res.json({ error: `no results found for testcase: ${widget.testcase}` })
    }
    else {
      widget.additionalInfo = result[0].readableOutcome
      widget.status = result[0].status
      widget.runDate = result[0].date
      widget.kpi = result[0].kpi.toUpperCase()
      widget.date = new Date()
      widget.data = result[0].data
      res.json(widget)
    }
  }

  if (widget.dataset === 'jira-filter') {
    jira(widget.jiraquery)
      .then((resp) => {
        widget.date = new Date()

        if (widget.type === 'enter-uat' || widget.type === 'exit-uat') {
          const countInArray = (array, value) => array.reduce((n, x) => n + (x === value), 0)
          const unique = resp.priorities.filter((item, index, array) => array.indexOf(item) === index)
          const amount = unique.map(field => countInArray(resp.priorities, field))
          const issues = []
          let permitted = true

          amount.forEach((issue, index) => {
            issues.push({ priority: unique[index], count: amount[index] })
          })

          widget.issues = issues

          const minorIssues = issues.find(issue => issue.priority === 'Minor')
          const majorIssues = issues.find(issue => issue.priority === 'Major')
          const criticalIssues = issues.find(issue => issue.priority === 'Critical' || issue.priority === 'Blocker')

          if (minorIssues && minorIssues.count > ((widget.type === 'enter-uat') ? 8 : 3)) {
            permitted = false
          }

          if (majorIssues && majorIssues.count > ((widget.type === 'enter-uat') ? 4 : 0)) {
            permitted = false
          }

          if (criticalIssues && majorIssues.count > 0) {
            permitted = false
          }

          widget.additionalInfo = `Minor: ${(minorIssues) ? minorIssues.count : 0}, Major: ${(majorIssues) ? majorIssues.count : 0}, Blocker: ${(criticalIssues) ? criticalIssues.count : 0}`
          widget.booleanStatus = permitted
        }

        widget.data = resp

        if (widget.charttype === 'pie') {
          widget.pieAditionalInfo = `Total tickets: ${resp.total}`
        }

        res.json(widget)
      })
      .catch((error) => {
        res.json({ error })
      })
  }
})

module.exports = router
