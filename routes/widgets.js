const router = require('express').Router()
const uuidv1 = require('uuid/v1')

router.get('/', (req, res) => {
  const widgets = req.database.getCollection('widgets').chain().simplesort('date', true).data()

  res.render('widgets/widgets', {
    title: 'Widgets',
    active: 'widgets',
    widgets
  })
})

router.get('/new', (req, res) => {
  res.render('widgets/new_widget', {
    title: 'New Widget',
    active: 'widgets'
  })
})

router.post('/new', (req, res) => {
  const toSave = {
    id: uuidv1(),
    title: req.body.title,
    type: req.body.type,
    description: req.body.description,
    dashboardtitle: req.body.dashboardtitle
  }

  const collection = req.database.getCollection('widgets')
  collection.insert(toSave)
  res.redirect(`/widgets/new/${toSave.type}/${toSave.id}`)
})

router.get('/new/:type/:id', (req, res) => {
  if (!req.params.id || req.params.id === '') {
    res.status(404)
    res.render('404', { active: '404' })
  }
  else {
    const widget = req.database.getCollection('widgets').find({ id: req.params.id })
    const testcases = req.database.getCollection('testcases').chain().simplesort('date', true).data()

    if (widget.length === 0) {
      res.status(404)
      res.render('404', { active: '404' })
    }
    res.render('widgets/new_widget_for_type', {
      title: 'Widget',
      active: 'widgets',
      widget: widget[0],
      testcases,
      jira: req.params.type.includes('jira') || req.params.type.includes('uat')
    })
  }
})

router.get('/edit/:type/:id', (req, res) => {
  if (!req.params.id || req.params.id === '') {
    res.status(404)
    res.render('404', { active: '404' })
  }
  else {
    const widget = req.database.getCollection('widgets').find({ id: req.params.id })
    const testcases = req.database.getCollection('testcases').chain().simplesort('date', true).data()

    if (widget.length === 0) {
      res.status(404)
      res.render('404', { active: '404' })
    }

    res.render('widgets/edit_widget', {
      title: 'Widget',
      active: 'widgets',
      widget: widget[0],
      testcases,
      jira: req.params.type.includes('jira') || req.params.type.includes('uat')
    })
  }
})

router.post('/new/jira-tickets', (req, res) => {
  const toSave = {
    id: req.body.id,
    description: req.body.description,
    title: req.body.title,
    dataset: req.body.dataset,
    jiraquery: req.body.jiraquery,
    dashboardtitle: req.body.dashboardtitle,
    charttype: req.body.charttype,
    chartfields: req.body.chartfields
  }

  const widget = req.database.getCollection('widgets').find({ id: toSave.id })

  if (widget.length === 0) {
    res.status(404)
    res.render('404', { active: '404' })
  }

  widget[0].title = toSave.title
  widget[0].description = toSave.description
  widget[0].dataset = toSave.dataset
  widget[0].jiraquery = toSave.jiraquery
  widget[0].dashboardtitle = toSave.dashboardtitle
  widget[0].charttype = toSave.charttype
  widget[0].chartfields = toSave.chartfields

  res.redirect('/widgets/')
})

router.post('/update/jira-tickets', (req, res) => {
  const toSave = {
    id: req.body.id,
    description: req.body.description,
    title: req.body.title,
    dataset: req.body.dataset,
    dashboardtitle: req.body.dashboardtitle,
    jiraquery: req.body.jiraquery,
    charttype: req.body.charttype,
    chartfields: req.body.chartfields
  }

  const collection = req.database.getCollection('widgets')
  const widget = collection.find({ id: toSave.id })

  if (widget.length === 0) {
    res.status(404)
    res.render('404', { active: '404' })
  }

  widget[0].title = toSave.title
  widget[0].description = toSave.description
  widget[0].dataset = toSave.dataset
  widget[0].dashboardtitle = toSave.dashboardtitle
  widget[0].jiraquery = toSave.jiraquery
  widget[0].charttype = toSave.charttype
  widget[0].chartfields = toSave.chartfields

  collection.update(widget)

  res.redirect('/widgets/')
})

router.post('/new/enter-uat', (req, res) => {
  const toSave = {
    id: req.body.id,
    description: req.body.description,
    title: req.body.title,
    dataset: req.body.dataset,
    dashboardtitle: req.body.dashboardtitle,
    jiraquery: req.body.jiraquery
  }

  const widget = req.database.getCollection('widgets').find({ id: toSave.id })

  if (widget.length === 0) {
    res.status(404)
    res.render('404', { active: '404' })
  }

  widget[0].title = toSave.title
  widget[0].description = toSave.description
  widget[0].dataset = toSave.dataset
  widget[0].dashboardtitle = toSave.dashboardtitle
  widget[0].jiraquery = toSave.jiraquery
  widget[0].charttype = 'boolean'

  res.redirect('/widgets/')
})

router.post('/update/enter-uat', (req, res) => {
  const toSave = {
    id: req.body.id,
    description: req.body.description,
    title: req.body.title,
    dataset: req.body.dataset,
    dashboardtitle: req.body.dashboardtitle,
    jiraquery: req.body.jiraquery
  }

  const collection = req.database.getCollection('widgets')
  const widget = collection.find({ id: toSave.id })

  if (widget.length === 0) {
    res.status(404)
    res.render('404', { active: '404' })
  }

  widget[0].title = toSave.title
  widget[0].description = toSave.description
  widget[0].dataset = toSave.dataset
  widget[0].dashboardtitle = toSave.dashboardtitle
  widget[0].jiraquery = toSave.jiraquery
  widget[0].charttype = 'boolean'

  collection.update(widget)

  res.redirect('/widgets/')
})

router.post('/new/exit-uat', (req, res) => {
  const toSave = {
    id: req.body.id,
    description: req.body.description,
    title: req.body.title,
    dataset: req.body.dataset,
    dashboardtitle: req.body.dashboardtitle,
    jiraquery: req.body.jiraquery
  }

  const widget = req.database.getCollection('widgets').find({ id: toSave.id })

  if (widget.length === 0) {
    res.status(404)
    res.render('404', { active: '404' })
  }

  widget[0].title = toSave.title
  widget[0].description = toSave.description
  widget[0].dataset = toSave.dataset
  widget[0].dashboardtitle = toSave.dashboardtitle
  widget[0].jiraquery = toSave.jiraquery
  widget[0].charttype = 'boolean'

  res.redirect('/widgets/')
})

router.post('/update/exit-uat', (req, res) => {
  const toSave = {
    id: req.body.id,
    description: req.body.description,
    title: req.body.title,
    dataset: req.body.dataset,
    dashboardtitle: req.body.dashboardtitle,
    jiraquery: req.body.jiraquery
  }

  const collection = req.database.getCollection('widgets')
  const widget = collection.find({ id: toSave.id })

  if (widget.length === 0) {
    res.status(404)
    res.render('404', { active: '404' })
  }

  widget[0].title = toSave.title
  widget[0].description = toSave.description
  widget[0].dataset = toSave.dataset
  widget[0].dashboardtitle = toSave.dashboardtitle
  widget[0].jiraquery = toSave.jiraquery
  widget[0].charttype = 'boolean'

  collection.update(widget)

  res.redirect('/widgets/')
})

router.post('/new/kpi-test-result', (req, res) => {
  const toSave = {
    id: req.body.id,
    description: req.body.description,
    title: req.body.title,
    dataset: req.body.dataset,
    testcase: req.body.testcase,
    dashboardtitle: req.body.dashboardtitle
  }

  const widget = req.database.getCollection('widgets').find({ id: toSave.id })

  if (widget.length === 0) {
    res.status(404)
    res.render('404', { active: '404' })
  }

  widget[0].title = toSave.title
  widget[0].description = toSave.description
  widget[0].dataset = toSave.dataset
  widget[0].testcase = toSave.testcase
  widget[0].dashboardtitle = toSave.dashboardtitle
  widget[0].charttype = 'kpi-pass-fail'

  res.redirect('/widgets/')
})

router.post('/update/kpi-test-result', (req, res) => {
  const toSave = {
    id: req.body.id,
    description: req.body.description,
    title: req.body.title,
    dataset: req.body.dataset,
    testcase: req.body.testcase,
    dashboardtitle: req.body.dashboardtitle
  }

  const collection = req.database.getCollection('widgets')
  const widget = collection.find({ id: toSave.id })

  if (widget.length === 0) {
    res.status(404)
    res.render('404', { active: '404' })
  }

  widget[0].title = toSave.title
  widget[0].description = toSave.description
  widget[0].dataset = toSave.dataset
  widget[0].testcase = toSave.testcase
  widget[0].dashboardtitle = toSave.dashboardtitle
  widget[0].charttype = 'kpi-pass-fail'

  collection.update(widget)

  res.redirect('/widgets/')
})

router.post('/new/blazemeter', (req, res) => {
  const toSave = {
    id: req.body.id,
    description: req.body.description,
    title: req.body.title,
    dataset: req.body.dataset,
    blazemeter_testid: req.body.blazemeter_testid,
    dashboardtitle: req.body.dashboardtitle
  }

  const widget = req.database.getCollection('widgets').find({ id: toSave.id })

  if (widget.length === 0) {
    res.status(404)
    res.render('404', { active: '404' })
  }

  widget[0].title = toSave.title
  widget[0].description = toSave.description
  widget[0].dataset = toSave.dataset
  widget[0].blazemeter_testid = toSave.blazemeter_testid
  widget[0].dashboardtitle = toSave.dashboardtitle
  widget[0].charttype = 'blazemeter'

  res.redirect('/widgets/')
})

router.post('/update/blazemeter', (req, res) => {
  const toSave = {
    id: req.body.id,
    description: req.body.description,
    title: req.body.title,
    dataset: req.body.dataset,
    blazemeter_testid: req.body.blazemeter_testid,
    dashboardtitle: req.body.dashboardtitle
  }

  const collection = req.database.getCollection('widgets')
  const widget = collection.find({ id: toSave.id })

  if (widget.length === 0) {
    res.status(404)
    res.render('404', { active: '404' })
  }

  widget[0].title = toSave.title
  widget[0].description = toSave.description
  widget[0].dataset = toSave.dataset
  widget[0].blazemeter_testid = toSave.blazemeter_testid
  widget[0].dashboardtitle = toSave.dashboardtitle
  widget[0].charttype = 'blazemeter'

  collection.update(widget)

  res.redirect('/widgets/')
})

router.post('/new/burndown', (req, res) => {
  const toSave = {
    id: req.body.id,
    description: req.body.description,
    title: req.body.title,
    dataset: req.body.dataset,
    jira_board_id: req.body.jira_board_id,
    jira_sprint_id: req.body.jira_sprint_id,
    dashboardtitle: req.body.dashboardtitle
  }

  const widget = req.database.getCollection('widgets').find({ id: toSave.id })

  if (widget.length === 0) {
    res.status(404)
    res.render('404', { active: '404' })
  }

  widget[0].title = toSave.title
  widget[0].description = toSave.description
  widget[0].dataset = toSave.dataset
  widget[0].jira_board_id = toSave.jira_board_id
  widget[0].dashboardtitle = toSave.dashboardtitle
  widget[0].jira_sprint_id = toSave.jira_sprint_id
  widget[0].charttype = 'burndown'

  res.redirect('/widgets/')
})

router.post('/update/burndown', (req, res) => {
  const toSave = {
    id: req.body.id,
    description: req.body.description,
    title: req.body.title,
    dataset: req.body.dataset,
    jira_board_id: req.body.jira_board_id,
    jira_sprint_id: req.body.jira_sprint_id,
    dashboardtitle: req.body.dashboardtitle
  }

  const collection = req.database.getCollection('widgets')
  const widget = collection.find({ id: toSave.id })

  if (widget.length === 0) {
    res.status(404)
    res.render('404', { active: '404' })
  }

  widget[0].title = toSave.title
  widget[0].description = toSave.description
  widget[0].dataset = toSave.dataset
  widget[0].jira_board_id = toSave.jira_board_id
  widget[0].dashboardtitle = toSave.dashboardtitle
  widget[0].jira_sprint_id = toSave.jira_sprint_id
  widget[0].charttype = 'burndown'

  collection.update(widget)

  res.redirect('/widgets/')
})

router.post('/new/selenium', (req, res) => {
  const toSave = {
    id: req.body.id,
    description: req.body.description,
    title: req.body.title,
    dataset: req.body.dataset,
    selenium_project: req.body.selenium_project,
    selenium_job: req.body.selenium_job,
    dashboardtitle: req.body.dashboardtitle,
    charttype: req.body.charttype
  }

  const widget = req.database.getCollection('widgets').find({ id: toSave.id })

  if (widget.length === 0) {
    res.status(404)
    res.render('404', { active: '404' })
  }

  widget[0].title = toSave.title
  widget[0].description = toSave.description
  widget[0].dataset = toSave.dataset
  widget[0].selenium_project = toSave.selenium_project
  widget[0].dashboardtitle = toSave.dashboardtitle
  widget[0].selenium_job = toSave.selenium_job
  widget[0].charttype = toSave.charttype

  res.redirect('/widgets/')
})

router.post('/update/selenium', (req, res) => {
  const toSave = {
    id: req.body.id,
    description: req.body.description,
    title: req.body.title,
    dataset: req.body.dataset,
    selenium_project: req.body.selenium_project,
    selenium_job: req.body.selenium_job,
    dashboardtitle: req.body.dashboardtitle,
    charttype: req.body.charttype
  }

  const collection = req.database.getCollection('widgets')
  const widget = collection.find({ id: toSave.id })

  if (widget.length === 0) {
    res.status(404)
    res.render('404', { active: '404' })
  }

  widget[0].title = toSave.title
  widget[0].description = toSave.description
  widget[0].dataset = toSave.dataset
  widget[0].selenium_project = toSave.selenium_project
  widget[0].dashboardtitle = toSave.dashboardtitle
  widget[0].selenium_job = toSave.selenium_job
  widget[0].charttype = toSave.charttype

  collection.update(widget)

  res.redirect('/widgets/')
})

router.post('/new/build-status', (req, res) => {
  const toSave = {
    id: req.body.id,
    description: req.body.description,
    title: req.body.title,
    dataset: req.body.dataset,
    build_project: req.body.build_project,
    build_job: req.body.build_job,
    dashboardtitle: req.body.dashboardtitle,
    charttype: 'kpi-pass-fail'
  }

  const collection = req.database.getCollection('widgets')
  const widget = collection.find({ id: toSave.id })

  if (widget.length === 0) {
    res.status(404)
    res.render('404', { active: '404' })
  }

  widget[0].title = toSave.title
  widget[0].description = toSave.description
  widget[0].dataset = toSave.dataset
  widget[0].build_project = toSave.build_project
  widget[0].build_job = toSave.build_job
  widget[0].dashboardtitle = toSave.dashboardtitle
  widget[0].charttype = toSave.charttype

  collection.update(widget)

  res.redirect('/widgets/')
})

router.post('/update/build-status', (req, res) => {
  const toSave = {
    id: req.body.id,
    description: req.body.description,
    title: req.body.title,
    dataset: req.body.dataset,
    build_project: req.body.build_project,
    build_job: req.body.build_job,
    dashboardtitle: req.body.dashboardtitle,
    charttype: 'kpi-pass-fail'
  }

  const collection = req.database.getCollection('widgets')
  const widget = collection.find({ id: toSave.id })

  if (widget.length === 0) {
    res.status(404)
    res.render('404', { active: '404' })
  }

  widget[0].title = toSave.title
  widget[0].description = toSave.description
  widget[0].dataset = toSave.dataset
  widget[0].build_project = toSave.build_project
  widget[0].build_job = toSave.build_job
  widget[0].dashboardtitle = toSave.dashboardtitle
  widget[0].charttype = toSave.charttype

  collection.update(widget)

  res.redirect('/widgets/')
})

router.get('/delete/:id', (req, res) => {
  if (!req.params.id || req.params.id === '') {
    res.status(404)
    res.render('404', { active: '404' })
  }
  else {
    const widget = req.database.getCollection('widgets').find({ id: req.params.id })

    if (widget.length === 0) {
      res.status(404)
      res.render('404', { active: '404' })
    }

    res.render('delete_item', {
      title: 'Delete widget',
      type: 'widgets',
      active: 'widgets',
      item: widget[0]
    })
  }
})

router.post('/delete/', (req, res) => {
  req.database.getCollection('widgets').chain().find({ id: req.body.id }).remove()
  res.redirect('/widgets/')
})

module.exports = router
