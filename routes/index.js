const router = require('express').Router()

router.get('/', (req, res) => {
  const testcases = req.database.getCollection('testcases').chain().simplesort('date', true).data()
  const results = req.database.getCollection('results').chain().simplesort('date', true).data()

  const passing = results.filter(result => result.status === 'PASS')
  const failing = results.filter(result => result.status === 'FAIL')
  const baseColors = ['#0d353f', '#2178a3', '#002a33', '#487d98', '#002a33', '#3a718b', '#002a33', '#4c707b', '#002a33',
    '#3f646e', '#002a33', '#245e78', '#002a33', '#074d65', '#002a33', '#2d525c', '#002a33', '#1b404a', '#002a33', '#002a33']

  const resultsGraphData = {
    type: 'doughnut',
    data: {
      labels: ['Passing tests', 'Failing tests'],
      datasets: [{
        label: 'bla',
        backgroundColor: baseColors,
        data: [passing.length, failing.length]
      }]
    },
    options: {
      legend: {
        display: true,
        labels: {
          fontColor: '#000',
          boxWidth: 10,
          fontSize: 11,
          padding: 10,
          fontFamily: "'Open Sans', sans-serif"
        }
      }
    }
  }


  res.render('index', {
    title: 'KPI test runner',
    active: 'home',
    testcases,
    results,
    passing,
    failing,
    resultsGraphData
  })
})

module.exports = router
