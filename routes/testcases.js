const router = require('express').Router()
const uuidv1 = require('uuid/v1')
const graphOptions = require('../modules/testcase-graph-options')

router.get('/', (req, res) => {
  const testcases = req.database.getCollection('testcases').chain().simplesort('date', true).data()

  res.render('testcases/testcases', {
    title: 'Test cases',
    active: 'testcases',
    testcases
  })
})

router.get('/new', (req, res) => {
  const testcases = req.database.getCollection('testcases').chain().simplesort('date', true).data()
  const projects = []
  for (const project of new Set(testcases.map((tc) => tc.project))) {
    projects.push(project)
  }

  res.render('testcases/new_testcase', {
    title: 'New Test case',
    active: 'testcases',
    projects
  })
})

router.post('/new', (req, res) => {
  const toSave = {
    id: uuidv1(),
    kpi: req.body.kpi,
    project: req.body.project,
    url: req.body.url,
    frequency: req.body.frequency,
    environment: req.body.environment,
    title: req.body.title,
    sitecore_stats_url: req.body.sitecore_stats_url,
    sitecore_username: req.body.sitecore_username,
    sitecore_password: req.body.sitecore_password,
    blazemeter_testid: req.body.blazemeter_testid
  }

  const collection = req.database.getCollection('testcases')
  collection.insert(toSave)
  res.redirect(`/testcases/view/${toSave.id}`)
})

router.post('/update', (req, res) => {
  const toSave = {
    id: req.body.id,
    project: req.body.project,
    url: req.body.url,
    frequency: req.body.frequency,
    environment: req.body.environment,
    title: req.body.title,
    sitecore_stats_url: req.body.sitecore_stats_url,
    sitecore_username: req.body.sitecore_username,
    sitecore_password: req.body.sitecore_password,
    blazemeter_testid: req.body.blazemeter_testid
  }

  const testcase = req.database.getCollection('testcases').find({ id: toSave.id })

  if (testcase.length === 0) {
    res.status(404)
    res.render('404', { active: '404' })
  }

  testcase[0].project = toSave.project
  testcase[0].url = toSave.url
  testcase[0].frequency = toSave.frequency
  testcase[0].environment = toSave.environment
  testcase[0].title = toSave.title
  testcase[0].sitecore_stats_url = toSave.sitecore_stats_url
  testcase[0].sitecore_username = toSave.sitecore_username
  testcase[0].sitecore_password = toSave.sitecore_password
  testcase[0].blazemeter_testid = toSave.blazemeter_testid

  res.redirect(`/testcases/view/${toSave.id}`)
})

router.get('/view/:id', (req, res) => {
  if (!req.params.id || req.params.id === '') {
    res.status(404)
    res.render('404', { active: '404' })
  }
  else {
    const testcase = req.database.getCollection('testcases').find({ id: req.params.id })
    const testcases = req.database.getCollection('testcases').chain().simplesort('id', true).data()
    const projects = []
    for (const project of new Set(testcases.map((tc) => tc.project))) {
      projects.push(project)
    }

    if (testcase.length === 0) {
      res.status(404)
      res.render('404', { active: '404' })
    }

    const results = req.database.getCollection('results')
      .chain()
      .find({
        testcase_id: testcase[0].id
      })
      .simplesort('id', true)
      .data()

    const resultsGraphOptions = graphOptions(testcase[0], results)
    const kpi = testcase[0].kpi

    res.render('testcases/testcase', {
      title: 'Testcase',
      active: 'testcases',
      testcase: testcase[0],
      projects,
      resultsGraphOptionsAvailable: (kpi === 'wp1' || kpi === 'wp4' || kpi === 'wp6'),
      graphOptions: resultsGraphOptions
    })
  }
})

router.get('/delete/:id', (req, res) => {
  if (!req.params.id || req.params.id === '') {
    res.status(404)
    res.render('404', { active: '404' })
  }
  else {
    const testcase = req.database.getCollection('testcases').find({ id: req.params.id })

    if (testcase.length === 0) {
      res.status(404)
      res.render('404', { active: '404' })
    }

    res.render('delete_item', {
      title: 'Delete result',
      type: 'testcases',
      active: 'testcases',
      item: testcase[0]
    })
  }
})

router.post('/delete/', (req, res) => {
  req.database.getCollection('testcases').chain().find({ id: req.body.id }).remove()
  res.redirect('/testcases/')
})

module.exports = router
