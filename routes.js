function create(app) {
  app.use('/', require('./routes/index'))
  app.use('/api', require('./routes/api'))
  app.use('/results', require('./routes/results'))
  app.use('/testcases', require('./routes/testcases'))
  app.use('/settings', require('./routes/settings'))
  app.use('/dashboards', require('./routes/dashboards'))
  app.use('/widgets', require('./routes/widgets'))
  app.use('/test', require('./routes/test'))

  app.use((req, res) => {
    res.status(404)
    res.render('404', { title: '404', message: 'This page does not exist.' })
  })
}

module.exports = { create }
