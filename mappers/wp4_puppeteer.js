module.exports = (data) => {
  const result = {
    timeUnit: 'ms',
    start: data.start,
    end: data.end,
    pageLoaded: data.end - data.start,
    navigationStart: data.timing.navigationStart - data.start,
    fetchStart: data.timing.fetchStart - data.start,
    domainLookupStart: data.timing.domainLookupStart - data.start,
    domainLookupEnd: data.timing.domainLookupEnd - data.start,
    connectStart: data.timing.connectStart - data.start,
    connectEnd: data.timing.connectEnd - data.start,
    secureConnectionStart: data.timing.secureConnectionStart - data.start,
    requestStart: data.timing.requestStart - data.start,
    responseStart: data.timing.responseStart - data.start,
    responseEnd: data.timing.responseEnd - data.start,
    domLoading: data.timing.domLoading - data.start,
    domInteractive: data.timing.domInteractive - data.start,
    domContentLoadedEventStart: data.timing.domContentLoadedEventStart - data.start,
    domContentLoadedEventEnd: data.timing.domContentLoadedEventEnd - data.start,
    domComplete: data.timing.domComplete - data.start,
    loadEventStart: data.timing.loadEventStart - data.start,
    loadEventEnd: data.timing.loadEventEnd - data.start
  }

  return result
}
