module.exports = (data) => {
  const getSummary = (metrics) => {
    const res = []
    metrics.forEach((metric, index) => {
      res.push({
        name: metric.name,
        result: data.items[0].web.summary.items[0].metrics[index]
      })
    })

    return res
  }

  const result = {
    catchpointId: data.id,
    runtime: data.items[0].runtime,
    node: data.items[0].node.name,
    monitor: data.items[0].monitor.name,
    summary: getSummary(data.items[0].web.summary.fields.metrics)
  }

  return result
}
