module.exports = (data) => {
  const result = data.audit.map(element => ({
    code: element.code,
    elements: element.elements,
    heading: element.heading,
    result: element.result,
    severity: element.severity,
    url: element.url
  }))

  return result
}
