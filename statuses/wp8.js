module.exports = (data) => {
  const errors = data.filter(page => (page.title.includes('404') || page.title.includes('500') || page.title.includes('error')))

  return {
    status: (errors.length > 0) ? 'FAIL' : 'PASS',
    readableOutcome: `There are ${errors.length} indexation errors.`
  }
}
