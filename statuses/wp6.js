module.exports = (data) => {
  const errors = data.filter(rule => rule.result === 'FAIL')
  return {
    status: (errors.length > 0) ? 'FAIL' : 'PASS',
    readableOutcome: `${errors.length} errors in ${data.length} assertions`
  }
}
