const getStatus = (d) => {
  let status = 'FAIL'

  if (d >= 3000 && d <= 4000) {
    status = 'WARNING'
  }

  if (d <= 3000) {
    status = 'PASS'
  }

  return status
}

module.exports = (data) => ({
  status: getStatus(data.summary[0].result),
  readableOutcome: `The test ${getStatus(data.summary[0].result) !== 'PASS' ? 'failed' : 'passed'} as ${data.summary[0].name} was ${data.summary[0].result}`
})
