const getStatus = (d) => {
  let status = 'FAIL'

  if (d.mobileFriendliness !== 'MOBILE_FRIENDLY') {
    status = 'WARNING'
  }

  if (d.mobileFriendliness === 'MOBILE_FRIENDLY') {
    status = 'PASS'
  }

  return status
}

module.exports = (data) => ({
  status: getStatus(data),
  readableOutcome: `This page is ${getStatus(data) === 'PASS' ? 'mobile friendly' : 'not reachable (IP blocked?)'}`
})
