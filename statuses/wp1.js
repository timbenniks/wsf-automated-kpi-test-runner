module.exports = (result) => ({
  status: (result.score > 99) ? 'PASS' : 'FAIL',
  readableOutcome: `Errors: ${result.error.percent}% Avg reponse time: ${(result.responseTime.avg / 1000).toFixed(2)} (${(result.score).toFixed(2)}%)`
})
