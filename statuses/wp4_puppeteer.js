module.exports = (data) => ({
  status: (data.domComplete > 4000) ? 'FAIL' : 'PASS',
  readableOutcome: `It took ${data.domComplete}ms to DOM completed.`
})
