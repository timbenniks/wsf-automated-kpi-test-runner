const path = require('path')
const express = require('express')
const expressHbs = require('express-handlebars')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const Loki = require('lokijs')
const routes = require('./routes')
const fs = require('fs')

// setup
const database = new Loki('database.loki', {
  autoload: true,
  autosave: true,
  autosaveInterval: 4000,
  autoloadCallback: () => {
    let results = database.getCollection('results')
    if (results === null) {
      results = database.addCollection('results', { unique: ['id'], autoupdate: true })
    }

    let testcases = database.getCollection('testcases')
    if (testcases === null) {
      testcases = database.addCollection('testcases', { unique: ['id'], autoupdate: true })
    }

    let dashboards = database.getCollection('dashboards')
    if (dashboards === null) {
      dashboards = database.addCollection('dashboards', { unique: ['id'], autoupdate: true })
    }

    let widgets = database.getCollection('widgets')
    if (widgets === null) {
      widgets = database.addCollection('widgets', { unique: ['id'], autoupdate: true })
    }

    let settings = database.getCollection('settings')
    if (settings === null) {
      settings = database.addCollection('settings', { unique: ['id'], autoupdate: true })
    }

    if (!settings.findOne({ id: 'settingsJiraUrl' })) { settings.insert({ id: 'settingsJiraUrl', value: process.env.JIRA_URL }) }
    if (!settings.findOne({ id: 'settingsJiraUsername' })) { settings.insert({ id: 'settingsJiraUsername', value: process.env.JIRA_USERNAME }) }
    if (!settings.findOne({ id: 'settingsJiraPassword' })) { settings.insert({ id: 'settingsJiraPassword', value: process.env.JIRA_PASSWORD }) }
    if (!settings.findOne({ id: 'settingsGoogleApikey' })) { settings.insert({ id: 'settingsGoogleApikey', value: process.env.GOOGLE_API_KEY }) }
    if (!settings.findOne({ id: 'catchpointClientId' })) { settings.insert({ id: 'catchpointClientId', value: process.env.CATCHPOINT_CLIENT_ID }) }
    if (!settings.findOne({ id: 'catchpointClientSecret' })) { settings.insert({ id: 'catchpointClientSecret', value: process.env.CATCHPOINT_CLIENT_SECRET }) }
  }
})

const app = express()

// settings
app.set('port', process.env.PORT || 3000)
app.set('views', path.join(__dirname, 'views'))
// view engine & main template
app.engine('.hbs', expressHbs({
  defaultLayout: 'template',
  extname: '.hbs',
  helpers: {
    ifequal(arg1, arg2, opts) {
      if (arg1 === arg2) {
        return opts.fn(this)
      }
      return opts.inverse(this)
    },
    ifnotequal(arg1, arg2, opts) {
      if (arg1 !== arg2) {
        return opts.fn(this)
      }
      return opts.inverse(this)
    },
    select(selected, options) {
      return options.fn(this).replace(new RegExp(`value="${selected}"`), '$& selected="selected"').replace(new RegExp(`>${selected}</option>`), ' selected="selected"$&')
    },
    json(context) {
      return JSON.stringify(context)
    },
    inline(filename) {
      return fs.readFileSync(filename, 'utf-8')
    }
  }
}))
app.set('view engine', '.hbs')

// middleware
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
app.use('/public', express.static('public'))

// loki db reference for the router
app.use((req, res, next) => {
  req.database = database
  next()
})

app.use((req, res, next) => {
  res.header('X-powered-by', 'Tims automated KPI tester and dashboard')
  res.header('Access-Control-Allow-Origin', '*')
  next()
})

// router
routes.create(app)

// server
app.listen(app.get('port'), () => {
  console.log(`Listening on http://localhost:${app.get('port')}`)
})
