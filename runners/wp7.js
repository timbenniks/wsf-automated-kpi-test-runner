const MobileFriendly = require('mobile-friendly')
const mapper = require('../mappers/wp7')
const status = require('../statuses/wp7')

module.exports = url => new Promise((resolve, reject) => {
  const result = {}
  const mobileFriendly = new MobileFriendly(url, { apiKey: process.env.GOOGLE_API_KEY })

  mobileFriendly.run()
    .then((res) => {
      const mappedData = mapper(res)
      const testStatus = status(mappedData)
      result.status = testStatus.status
      result.readableOutcome = testStatus.readableOutcome
      resolve(result)
    })
    .catch(reject)
})
