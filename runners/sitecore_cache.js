const blazeMeter = require('../modules/blazemeter')
const mapper = require('../mappers/wp1')
const status = require('../statuses/wp1')

module.exports = id => new Promise((resolve, reject) => {
  const result = {}

  blazeMeter(id, (error, reports) => {
    if (error) {
      reject({ error })
    }
    else {
      const mappedData = mapper(reports)
      const testStatus = status(mappedData)
      result.status = testStatus.status
      result.readableOutcome = testStatus.readableOutcome
      result.data = mappedData
      resolve(result)
    }
  })
})
