const Walker = require('puppeteer-walker')
const mapper = require('../mappers/wp8')
const status = require('../statuses/wp8')

module.exports = url => new Promise((resolve) => {
  const walker = Walker()
  const result = {}
  const results = []

  walker.on('error', (error) => {
    results.push({ error, title: 'error' })
  })

  walker.on('page', async (page) => {
    const pageTitle = await page.title()
    const pageUrl = await page.url()
    console.log(pageUrl)
    results.push({ url: pageUrl, title: pageTitle })
  })

  walker.on('end', () => {
    const mappedData = mapper(results)
    const testStatus = status(mappedData)
    result.status = testStatus.status
    result.readableOutcome = testStatus.readableOutcome
    resolve(result)
  })

  walker.walk(url)
})
