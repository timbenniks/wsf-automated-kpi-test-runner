const mapper = require('../mappers/wp4_puppeteer')
const status = require('../statuses/wp4_puppeteer')
const puppeteer = require('puppeteer')

let start
let end

module.exports = async (url, result, collection, res) => {
  start = Date.now()
  const browser = await puppeteer.launch()
  const page = await browser.newPage()
  await page.setRequestInterception(true)
  page.on('load', () => { end = Date.now() })
  page.on('request', interceptedRequest => { interceptedRequest.continue() })
  await page._client.send('Performance.enable') // eslint-disable-line
  await page.goto(url)
  const timing = await page.evaluate(() => window.performance.toJSON())
  await browser.close()

  result.data = mapper({ start, end, timing: timing.timing })
  result.status = status(mapper({ start, end, timing: timing.timing })).status
  result.readableOutcome = status(mapper({ start, end, timing: timing.timing })).readableOutcome

  collection.insert(result)
  res.redirect(`/api/result/${result.id}`)
}
