const mapper = require('../mappers/wp4')
const status = require('../statuses/wp4')
const CatchPointAPI = require('../modules/catchpoint')

const catchPoint = new CatchPointAPI()

module.exports = url => new Promise((resolve, reject) => {
  const formResult = data => {
    const result = {}
    const mappedData = mapper(data)
    const testStatus = status(mappedData)
    result.status = testStatus.status
    result.readableOutcome = testStatus.readableOutcome
    result.data = mappedData

    resolve(result)
  }

  const getById = id => {
    catchPoint.ondemand.getById(id)
      .then(resp => {
        if (resp.items.length === 0) {
          setTimeout(() => { getById(id) }, 2000)
        }
        else {
          formResult(resp)
        }
      })
      .catch(reject)
  }

  const getTestResult = () => {
    catchPoint.ondemand.create(url)
      .then(response => { getById(response.id) })
      .catch(error => {
        if (error === 'missing access token') {
          catchPoint.authorization
            .createSession({ clientId: process.env.CATCHPOINT_CLIENT_ID, clientSecret: process.env.CATCHPOINT_CLIENT_SECRET })
            .then(response => catchPoint.setAccessToken(response.access_token))
            .then(getTestResult)
        }
      })
  }

  return getTestResult()
})
