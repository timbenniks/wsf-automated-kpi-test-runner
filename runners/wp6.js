const a11y = require('a11y')
const mapper = require('../mappers/wp6')
const status = require('../statuses/wp6')

module.exports = url => new Promise((resolve, reject) => {
  const result = {}

  a11y(url, { delay: 2 }, (error, reports) => {
    if (error) {
      reject({ error })
    }
    else {
      const mappedData = mapper(reports)
      const testStatus = status(mappedData)
      result.status = testStatus.status
      result.readableOutcome = testStatus.readableOutcome
      result.data = mappedData
      resolve(result)
    }
  })
})
